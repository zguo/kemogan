# KemoGAN

Source code for Furry Image Generation with Generative Adversarial Networks.

### Credits

StyleGAN2 training framework:
[https://github.com/NVlabs/stylegan2-ada-pytorch](https://github.com/NVlabs/stylegan2-ada-pytorch)

LPIPS:
[https://github.com/richzhang/PerceptualSimilarity](https://github.com/richzhang/PerceptualSimilarity)

E621-Face-Dataset:
[https://github.com/arfafax/E621-Face-Dataset](https://github.com/arfafax/E621-Face-Dataset)