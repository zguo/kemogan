import sys
import os
sys.path.append(os.getcwd())

import torch
from training.networks import Generator
from PIL import Image
import numpy as np
import pickle
import copy

def make_noise(size, device):
    return torch.randn(size, device=device)   # [N, 512] in Z space

def layer_swapping(source: Generator, target: Generator, device, ls_res: int, G_kwargs, swap_generators=False):
    G_ret = Generator(**G_kwargs).to(device).eval()

    G_ret.mapping = source.mapping  # use the mapping network from the source generator
    resolutions = [2 ** i for i in range(2, 9)]
    for res in resolutions:
        if res <= ls_res:
            synthesis = source.synthesis if swap_generators else target.synthesis
            setattr(G_ret.synthesis, f'b{res}', copy.deepcopy(synthesis.get_block(res)))
        else:
            synthesis = target.synthesis if swap_generators else source.synthesis
            setattr(G_ret.synthesis, f'b{res}', copy.deepcopy(synthesis.get_block(res)))
    return G_ret

def g_result_to_pil(var):
    var = var.cpu().detach().permute(1, 2, 0) * 127.5 + 128  # [CHW] to [HWC]
    var = var.clamp(0, 255).to(torch.uint8)
    return Image.fromarray(var.numpy())

def save_image_grid(img, fname, drange, grid_size):
    lo, hi = drange
    img = np.asarray(img, dtype=np.float32)
    img = (img - lo) * (255 / (hi - lo))
    img = np.rint(img).clip(0, 255).astype(np.uint8)

    gw, gh = grid_size
    _N, C, H, W = img.shape
    img = img.reshape(gh, gw, C, H, W)
    img = img.transpose(0, 3, 1, 4, 2)
    img = img.reshape(gh * H, gw * W, C)

    assert C in [1, 3]
    if C == 1:
        Image.fromarray(img[:, :, 0], 'L').save(fname)
    if C == 3:
        Image.fromarray(img, 'RGB').save(fname)

if __name__ == '__main__':
    G_kwargs = {'c_dim': 0, 'img_resolution': 256, 'img_channels': 3, 'z_dim': 512, 'w_dim': 512, 'mapping_kwargs': {'num_layers': 8}, 'synthesis_kwargs': {'channel_base': 16384, 'channel_max': 512, 'num_fp16_res': 4, 'conv_clamp': 256}}
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    G_s = Generator(**G_kwargs).to(device).eval()
    G_t = Generator(**G_kwargs).to(device).eval()
    # load weights
    with open('kemogan/pre-trained/ffhq-res256-mirror-paper256-noaug.pkl', 'rb') as f:
        G_s_dict = pickle.load(f)['G_ema'].state_dict()
    G_s.load_state_dict(G_s_dict, strict=True)
    weight_files = ['final']
    for weight_file in weight_files:
        with open(f'kemogan/checkpoints/{weight_file}.pkl', 'rb') as f:
            G_t_dict = pickle.load(f)['G_ema'].state_dict()
        G_t.load_state_dict(G_t_dict, strict=True)
        # make noise
        noise_z = make_noise([9, 512], device)
        # generate the initial images using the source generator
        imgs = G_s(noise_z, c=None, noise_mode='const', force_fp32=True)
        save_image_grid(imgs.to('cpu').detach(), os.path.join(f'kemogan/layer_swapping/{weight_file}', 'initial.png'), drange=[-1,1], grid_size=(3, 3))
        # make directories
        os.makedirs(f'kemogan/layer_swapping/{weight_file}', exist_ok=True)
        with torch.no_grad():
            resolutions = [2 ** i for i in range(2, 9)]
            for res in resolutions:
                # layer swapping
                G_i = layer_swapping(G_s, G_t, device, res, G_kwargs, False)
                imgs = G_i(noise_z, c=None, noise_mode='const', force_fp32=True)
                save_image_grid(imgs.to('cpu').detach(), os.path.join(f'kemogan/layer_swapping/{weight_file}', f'{res}.png'), drange=[-1,1], grid_size=(3, 3))
                G_i = layer_swapping(G_s, G_t, device, res, G_kwargs, True)
                imgs = G_i(noise_z, c=None, noise_mode='const', force_fp32=True)
                save_image_grid(imgs.to('cpu').detach(), os.path.join(f'kemogan/layer_swapping/{weight_file}', f'{res}_rev_int.png'), drange=[-1,1], grid_size=(3, 3))
    print('Done.')