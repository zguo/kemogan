import sys
import os
sys.path.append(os.getcwd())

import torch
from training.networks import Generator
from PIL import Image
import numpy as np
import pickle
import copy

def make_noise(size, device):
    return torch.randn(size, device=device)   # [N, 512] in Z space

def layer_swapping(source: Generator, target: Generator, device, ls_res: int, G_kwargs, swap_generators=False):
    G_ret = Generator(**G_kwargs).to(device).eval()

    G_ret.mapping = source.mapping  # use the mapping network from the source generator
    resolutions = [2 ** i for i in range(2, 9)]
    for res in resolutions:
        if res <= ls_res:
            synthesis = source.synthesis if swap_generators else target.synthesis
            setattr(G_ret.synthesis, f'b{res}', copy.deepcopy(synthesis.get_block(res)))
        else:
            synthesis = target.synthesis if swap_generators else source.synthesis
            setattr(G_ret.synthesis, f'b{res}', copy.deepcopy(synthesis.get_block(res)))
    return G_ret

def g_result_to_pil(var):
    var = var.cpu().detach().permute(1, 2, 0) * 127.5 + 128  # [CHW] to [HWC]
    var = var.clamp(0, 255).to(torch.uint8)
    return Image.fromarray(var.numpy())

def save_image_grid(img, fname, drange, grid_size):
    lo, hi = drange
    img = np.asarray(img, dtype=np.float32)
    img = (img - lo) * (255 / (hi - lo))
    img = np.rint(img).clip(0, 255).astype(np.uint8)

    gw, gh = grid_size
    _N, C, H, W = img.shape
    img = img.reshape(gh, gw, C, H, W)
    img = img.transpose(0, 3, 1, 4, 2)
    img = img.reshape(gh * H, gw * W, C)

    assert C in [1, 3]
    if C == 1:
        Image.fromarray(img[:, :, 0], 'L').save(fname)
    if C == 3:
        Image.fromarray(img, 'RGB').save(fname)

if __name__ == '__main__':
    G_kwargs = {'c_dim': 0, 'img_resolution': 256, 'img_channels': 3, 'z_dim': 512, 'w_dim': 512, 'mapping_kwargs': {'num_layers': 8}, 'synthesis_kwargs': {'channel_base': 16384, 'channel_max': 512, 'num_fp16_res': 4, 'conv_clamp': 256}}
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    # make noise in z space
    noise_z = make_noise([9, 512], device)
    # construct the source generator
    G_s = Generator(**G_kwargs).to(device).eval()
    with open('kemogan/pre-trained/ffhq-res256-mirror-paper256-noaug.pkl', 'rb') as f:
        G_s_dict = pickle.load(f)['G_ema'].state_dict()
    G_s.load_state_dict(G_s_dict, strict=True)
    # generator initial fake images
    os.makedirs(f'kemogan/evaluate_formal', exist_ok=True)
    with torch.no_grad():
        imgs = G_s(noise_z, c=None, noise_mode='const', force_fp32=True)
        save_image_grid(imgs.to('cpu').detach(), 'kemogan/evaluate_formal/initial.png', drange=[-1,1], grid_size=(3, 3))
    # load weights
    weight_files = ['none_formal_300', 'struct_0_5_formal_300', 'struct_1_0_formal_300']
    for weight_file in weight_files:
        G_t = Generator(**G_kwargs).to(device).eval()
        with open(f'kemogan/checkpoints/{weight_file}.pkl', 'rb') as f:
            state_dict = pickle.load(f)['G_ema'].state_dict()
        G_t.load_state_dict(state_dict, strict=True)
        with torch.no_grad():
            imgs = G_t(noise_z, c=None, noise_mode='const', force_fp32=True)
            save_image_grid(imgs.to('cpu').detach(), f'kemogan/evaluate_formal/{weight_file}.png', drange=[-1,1], grid_size=(3, 3))
    print('Done.')